import {
    SEND_APPLICATION,

    GET_APPLICATIONS_PENDING,
    GET_APPLICATIONS_SUCCESS,

    REMOVE_APPLICATION,
} from '../types/application';

export const sendApplication = data => ({
    type: SEND_APPLICATION,
    data
});

export const getApplications = () => ({
    type: GET_APPLICATIONS_PENDING,
});

export const removeApplication = id => ({
    type: REMOVE_APPLICATION,
    id
});

export const onGetApplicationsSuccess = data => ({
    type: GET_APPLICATIONS_SUCCESS,
    data
});