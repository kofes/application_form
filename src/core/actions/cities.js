import API from '../api';

import {
    CITIES_LIST_PENDING,
    CITIES_LIST_SUCCESS,
    CITIES_LIST_ERROR,
    CITIES_LIST_EXCEPTION,

    CITY_SELECT,
} from '../types/cities';

export const getCities = () => dispatch => {
    dispatch(onGetCitiesPending());
    return API.get('/5b34c0d82f00007400376066?mocky-delay=700ms')
    .then(
        res => dispatch(onGetCitiesSuccess(res.data.cities)),
        err => dispatch(onGetCitiesError(err))
    )
    .catch(ex => dispatch(onGetCitiesException(ex)));
};

export const onGetCitiesPending = () => ({
    type: CITIES_LIST_PENDING
});

export const onGetCitiesSuccess = data => ({
    type: CITIES_LIST_SUCCESS,
    data,
});

export const onGetCitiesError = error => ({
    type: CITIES_LIST_ERROR,
    error,
});

export const onGetCitiesException = exception => ({
    type: CITIES_LIST_EXCEPTION,
    exception,
});

export const selectCity = cityID => ({
    type: CITY_SELECT,
    cityID,
});