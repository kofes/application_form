import API from '../api';

import {
    DAYLIST_PENDING,
    DAYLIST_SUCCESS,
    DAYLIST_ERROR,
    DAYLIST_EXCEPTION,
} from '../types/daylist';

export const getDayList = cityID => dispatch => {
    dispatch(onGetDayListPending(cityID));
    return API.get(`${cityID}?mocky-delay=700ms`)
        .then(
            res => dispatch(onGetDayListSuccess(cityID, res.data.data)),
            err => dispatch(onGetDayListError(cityID, err))
        )
        .catch(ex => dispatch(onGetDayListException(cityID, ex)));
};

export const onGetDayListPending = cityID => ({
    type: DAYLIST_PENDING,
    cityID
});

export const onGetDayListSuccess = (cityID, data) => ({
    type: DAYLIST_SUCCESS,
    cityID,
    data,
});

export const onGetDayListError = (cityID, error) => ({
    type: DAYLIST_ERROR,
    cityID,
    error,
});

export const onGetDayListException = (cityID, exception) => ({
    type: DAYLIST_EXCEPTION,
    cityID,
    exception,
});