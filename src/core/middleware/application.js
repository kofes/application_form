import {
    SEND_APPLICATION,
    GET_APPLICATIONS_PENDING,
    REMOVE_APPLICATION,
} from '../types/application';

import {
    onGetApplicationsSuccess
} from '../actions/application';

export const sending = store => next => action => {
    const { type, ...payload } = action;
    switch (type) {
        case SEND_APPLICATION: {
            const name = 'orders';
            const str = localStorage.getItem(name);
            const dict = str && str.length ? JSON.parse(str) : {};
            dict[`${payload.data.date} ${payload.data.time}`] = payload.data;
            localStorage.setItem(name, JSON.stringify(dict));
            return next(action);
        }
        default: return next(action);
    }
};

export const receiving = store => next => action => {
    const { type, ...payload } = action;
    switch (type) {
        case GET_APPLICATIONS_PENDING: {
            const name = 'orders';
            const str = localStorage.getItem(name);
            const dict = str && str.length ? JSON.parse(str) : {};
            store.dispatch(onGetApplicationsSuccess(dict));
            return next(action);
        }
        default: return next(action);
    }
};

export const removing = store => next => action => {
    const {type, ...payload} = action;
    switch (type) {
        case REMOVE_APPLICATION: {
            const name = 'orders';
            const str = localStorage.getItem(name);
            const dict = str && str.length ? JSON.parse(str) : {};
            if (dict.hasOwnProperty(payload.id)) {
                delete dict[payload.id];
                localStorage.setItem(name, JSON.stringify(dict));
                store.dispatch(onGetApplicationsSuccess(dict));
            }
            return next(action);
        };
        default: return next(action);
    }
};