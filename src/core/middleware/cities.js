import {
    CITIES_LIST_PENDING,
    CITIES_LIST_SUCCESS,

    CITY_SELECT,
} from '../types/cities';

import {
    INITIAL_STATE
} from '../reducers/cities';

export const pending = store => next => action => {
    const { type, ...payload } = action;
    switch (type) {
        case CITIES_LIST_PENDING: {
            return next({
                ...action,
                data: JSON.parse(localStorage.getItem('cities')) || payload.data || INITIAL_STATE.data,
            });
        }
        default: return next(action);
    }
};

export const success = store => next => action => {
    const { type, ...payload } = action;
    switch (type) {
        case CITIES_LIST_SUCCESS: {
            localStorage.setItem('cities', JSON.stringify(payload.data));
            return next(action);
        }
        default: return next(action);
    }
};

export const citySelected = store => next => action => {
    const { type, ...payload } = action;
    switch (type) {
        case CITY_SELECT: {
            localStorage.setItem('selectedCityID', payload.cityID);
            return next(action);
        }
        default: return next(action);
    }
};