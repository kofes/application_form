import {
    DAYLIST_PENDING,
    DAYLIST_SUCCESS,
} from '../types/daylist';

import {
    INITIAL_STATE
} from '../reducers/daylist';

export const pending = store => next => action => {
    const { type, ...payload } = action;
    switch (type) {
        case DAYLIST_PENDING: {
            const daylist = JSON.parse(localStorage.getItem(`daylist_${action.cityID}`));
            return next({
                ...action,
                type: daylist ? DAYLIST_SUCCESS : type,
                data: daylist || payload.data || INITIAL_STATE.data,
            });
        }
        default: return next(action);
    }
};

export const success = store => next => action => {
    const { type, ...payload } = action;
    switch (type) {
        case DAYLIST_SUCCESS: {
            localStorage.setItem(`daylist_${action.cityID}`, JSON.stringify(payload.data));
            return next(action);
        }
        default: return next(action);
    }
};