import { applyMiddleware } from 'redux';
import { middleware as fetchMiddleware } from "react-redux-fetch";

import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {
    pending as daylistPending,
    success as daylistSuccess,
} from '../middleware/daylist';
import {
    pending as citiesPending,
    success as citiesSuccess,
    citySelected
} from '../middleware/cities';

import {
    sending as applicationsSending,
    receiving as applicationsReceiving,
    removing as applicationRemoving,
} from '../middleware/application';

const middleware = [
    thunk,

    citiesPending,
    citiesSuccess,
    citySelected,

    daylistPending,
    daylistSuccess,

    applicationsSending,
    applicationsReceiving,
    applicationRemoving,
];

if (process.env.NODE_ENV === 'development') {
    middleware.push(logger);
}

export default applyMiddleware(
    fetchMiddleware,
    ...middleware
);