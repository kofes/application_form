import {
    GET_APPLICATIONS_PENDING,
    GET_APPLICATIONS_SUCCESS,
} from '../types/application';

const INITIAL_STATE = {
    data: {},
    pending: true,
};

const applications = (state = INITIAL_STATE, {type, ...payload}) => {
    switch (type) {
        case GET_APPLICATIONS_SUCCESS: return {
            ...state,
            data: payload.data,
            pending: false,
        }
        default: return state;
    };
};

export default applications;