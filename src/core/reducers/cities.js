import {
    CITIES_LIST_PENDING,
    CITIES_LIST_SUCCESS,
    CITIES_LIST_ERROR,
    CITIES_LIST_EXCEPTION,

    CITY_SELECT,
} from '../types/cities';

export const INITIAL_STATE = {
    data: [],
    cityID: '',
    exception: '',
    error: '',
    pending: true,
};

const cities = (state = INITIAL_STATE, {type, ...payload}) => {
    switch (type) {
        case CITIES_LIST_PENDING: return {
            ...state,
            ...payload,
            pending: true,
        };
        case CITIES_LIST_SUCCESS: return {
            ...state,
            ...payload,
            pending: false,
        };
        case CITIES_LIST_ERROR: return {
            ...state,
            error: payload.error,
            pending: false,
        };
        case CITIES_LIST_EXCEPTION: return {
            ...state,
            exception: payload.exception,
            pending: false,
        };
        case CITY_SELECT: return {
            ...state,
            cityID: payload.cityID,
        };
        default: return state;
    }
};

export default cities;