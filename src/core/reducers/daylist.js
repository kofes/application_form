import {
    DAYLIST_PENDING,
    DAYLIST_SUCCESS,
    DAYLIST_ERROR,
    DAYLIST_EXCEPTION,
} from '../types/daylist';

export const INITIAL_STATE = {
    data: {},
    exception: '',
    error: '',
    pending: true,
};

const daylist = (state = INITIAL_STATE, { type, ...payload }) => {
    switch (type) {
        case DAYLIST_PENDING: return {
            ...state,
            data: payload.data,
            pending: true,
        };
        case DAYLIST_SUCCESS: return {
            ...state,
            data: payload.data,
            pending: false,
        };
        case DAYLIST_ERROR: return {
            ...state,
            error: payload.error,
            pending: false,
        };
        case DAYLIST_EXCEPTION: return {
            ...state,
            exception: payload.exception,
            pending: false,
        };
        default: return state;
    }
};

export default daylist;