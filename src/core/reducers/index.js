import { combineReducers } from 'redux';
import { reducer as fetchReducer } from 'react-redux-fetch';

import cities from './cities';
import daylist from './daylist';
import applications from './applications';

const reducers = {
    cities,
    daylist,
    applications,
};

export default combineReducers({
    ...reducers,
    fetchReducer,
});