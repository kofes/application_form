export const SEND_APPLICATION = 'SEND_APPLICATION';
export const GET_APPLICATIONS_PENDING = 'GET_APPLICATIONS_PENDING'; 
export const GET_APPLICATIONS_SUCCESS = 'GET_APPLICATIONS_SUCCESS';
export const REMOVE_APPLICATION = 'REMOVE_APPLICATION';