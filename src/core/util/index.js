export const camelToSnake = string => {
    return string.replace(/[\w]([A-Z])/g, m => {
        return m[0] + "_" + m[1];
    }).toLowerCase();
};

export const formatPhone = phone => {
    return `+${phone[0]} (${phone.slice(1, 4)}) ${phone.slice(4, 7)}-${phone.slice(7, 9)}-${phone.slice(9-11)}`
};


export const capitalize = str => str.charAt(0).toUpperCase() + str.slice(1);

export const formatDate = rawDate => {
    const year = Number(rawDate.slice(0, 4));
    const month = Number(rawDate.slice(5, 7));
    const day = Number(rawDate.slice(8, 10));

    const date = new Date(year, month - 1, day);

    const dayStr = date.toLocaleDateString('ru-RU', { day: 'numeric' });
    const monthStr = capitalize(date.toLocaleDateString('ru-RU', { month: 'long'}));
    const weekdayStr = capitalize(date.toLocaleDateString('ru-RU', { weekday: 'long' }));

    return `${weekdayStr}, ${dayStr} ${monthStr}`;
};

export const filterDateTime = datetime => {
    let result = {};
    for (let key in datetime) {
        if (datetime.hasOwnProperty(key)) {
            for (let innerKey in datetime[key]) {
                if (datetime[key].hasOwnProperty(innerKey) && !datetime[key][innerKey].is_not_free) {
                    result[key] = result[key] || {};
                    result[key][innerKey] = datetime[key][innerKey];
                }
            }
        }
    }
    return result;
};