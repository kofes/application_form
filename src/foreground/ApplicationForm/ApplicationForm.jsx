import React, {Component} from 'react';
import PropTypes from 'prop-types';

import logo from './logo.svg';
import modifiedLogo from './modifiedLogo.svg';
import './ApplicationForm.styl';

import {camelToSnake, formatPhone, filterDateTime, formatDate} from '../../core/util';

import Dropdown from "../Dropdown";

const INITIAL_STATE = {
    date: null,
    time: null,
    valid: {
        date: true,
        time: true,
        phone: true,
        name: true,
    },
    phone: '',
    name: '',
    citySelected : false,
    cityID: '',
};

class ApplicationForm extends Component {
    constructor(props) {
        super(props);
        this.state = INITIAL_STATE;
    }

    componentDidMount() {
        const { getCities } = this.props;
        getCities();
    }

    onCitySelect(cityID) {        
        const { onCitySelect } = this.props;
        this.setState({
            ...this.state,
            valid: {
                ...INITIAL_STATE.valid
            },
            date: null,
            time: null,
        });
        onCitySelect(cityID);
    }

    componentDidUpdate(prevProps, prevStates) {
        if (!prevProps.cities.length && this.props.cities.length > 0) {
            const { onCitySelect } = this.props;
            this.onCitySelect(this.props.cities[0].id);
        }
    }

    onSelect(name, value) {
        this.setState({
            ...this.state,
            [name]: value,
            valid: {
                ...this.state.valid,
                [name]: true,
            }
        });
    }

    onValidationFail(name) {
        this.setState({
            valid: {
                ...this.state.valid,
                [name]: false
            }
        });
    }

    onInputChange(name, value) {
        this.setState({
            ...this.state,
            [name]: value,
        })
    }

    validateInput(name) {
        let success = true;
        const value = this.state[name];
        switch (name) {
            case 'phone': {
                if (!value || !value.length || !/(?:\+|\d)[\d\-\(\) ]{10,}\d/.test(value)) {
                    success = false;
                }
                break;
            }
            case 'name': {
                if (!value || !value.length) {
                    success = false;
                }
                break;
            }
            default: return;
        }
        this.setState({
            ...this.state,
            valid: {
                ...this.state.valid,
                [name]: success,
            }
        });
    }

    sendApplication() {
        const { cities, cityID, datetime, onSendApplication } = this.props;
        const { date, time } = this.state;

        const city = cityID.length ? cities.find(obj => obj.id === cityID) : null;

        onSendApplication({
            city,
            date,
            time,
        });
    }

    isComplete() {
        if (this.state['phone']) {
            const value = this.state['phone'];
            if (!value || !value.length || !/(?:\+|\d)[\d\-\(\) ]{10,}\d/.test(value)) {
                return false;
            }
        } else { return false; }
        if (this.state['name']) {
            const value = this.state['name'];
            if (!value || !value.length) {
                return false;
            }
        } else { return false; }
        
        const {date, time, valid} = this.state;
        if (date === null || time === null || !valid.date || !valid.time) {
            return false;
        }

        return true;
    }

    render() {
        const { cities, cityID, datetime, citiesPending, daylistPending } = this.props;
        const { date, time, valid } = this.state;

        const selectedCity = cityID.length ? cities.find(obj => obj.id === cityID) : null;
        const selectedCityIndex = cities.indexOf(selectedCity);

        const filteredDateTime = filterDateTime(datetime);

        const dates = Object.keys(filteredDateTime);
        const filteredDates = dates.map(elem => formatDate(elem));

        const filteredTimes = date && filteredDateTime.hasOwnProperty(date) ? Object.entries(filteredDateTime[date]).map(elem => `${elem[1].begin}-${elem[1].end}`) : [];

        const pending = citiesPending || daylistPending;

        const cityDescriptionLayout = selectedCity ? (
            <div className='city_description'>
                <p className='address'>{selectedCity.address}</p>
                <p className='phones'>{selectedCity.phones.map((elem, idx) => (
                    <a key={idx} href={`tel:${formatPhone(elem)}`}>{formatPhone(elem)}</a>
                ))}</p>
                <p className='price'>Стоимость услуги {selectedCity.price} ₽</p>
            </div>
        ) : <React.Fragment/>;

        return (
            <div className={camelToSnake(this.constructor.name)}>
                <div className='logo_container'>
                    <div className={`loader${pending ? '' : ' invisible'}`}/>
                    <img src={logo} className={`logo${pending ? ' invisible' : ''}`}/>
                    <img src={modifiedLogo} className={`logo${pending ? '' : ' invisible'}`}/>
                </div>
                <h3>Онлайн запись</h3>
                <Dropdown
                    clickable={cities.length > 0}
                    selected={selectedCityIndex >= 0 ? selectedCityIndex : 0}
                    data={cities.map(elem => elem.name)}
                    onSelect={index => this.onCitySelect(cities[index].id)}
                    placeholder='Город'
                />
                {cityDescriptionLayout}
                <div className={'datetime'}>
                    <Dropdown
                        validation={true}
                        onValidationFail={() => this.onValidationFail('date')}
                        selected={dates.indexOf(date)}
                        clickable={filteredDates.length > 0}
                        placeholder='Дата'
                        data={filteredDates}
                        onSelect={index => this.onSelect('date', dates[index])}
                    />
                    <Dropdown
                        validation={true}
                        onValidationFail={() => this.onValidationFail('time')}
                        clickable={date !== null && filteredTimes.length > 0}
                        selected={filteredTimes.indexOf(time)}
                        placeholder='Время'
                        data={filteredTimes}
                        onSelect={index => this.onSelect('time', filteredTimes[index])}
                    />
                </div>
                {!valid.date ? <p className='validation_error'>{'Пожалуста, выберите дату'}</p> : <React.Fragment/>}
                {!valid.time ? <p className='validation_error'>{'Пожалуста, выберите время'}</p> : <React.Fragment/>}
                <div className='input'>
                    <input
                        className={`phone${!valid.phone ? ' invalid' : ''}`}
                        type='text'
                        placeholder='+7 (___) ___-__-__'
                        value={this.state.phone}
                        onChange={event => this.onInputChange('phone', event.target.value)}
                        onBlur={() => this.validateInput('phone')}
                    />
                </div>
                {!valid.phone ? <p className='validation_error'>{'Пожалуйста, введите корректный телефон, иначе наши специалисты не смогут связаться с вами'}</p> : <React.Fragment/>}
                <div className='input'>
                    <input
                        className={`name${!valid.name ? ' invalid' : ''}`}
                        type='text'
                        placeholder='Ваше имя'
                        value={this.state.name}
                        onChange={event => this.onInputChange('name', event.target.value)}
                        onBlur={() => this.validateInput('name')}
                    />
                </div>
                {!valid.name ? <p className='validation_error'>{'Пожалуйста, укажите имя'}</p> : <React.Fragment/>}
                <div className='button_holder'>
                    <button
                        onClick={() => this.sendApplication()}
                        disabled={!this.isComplete()}
                    >
                        Записаться
                    </button>
                </div>
                <div className='agreement_holder'>
                    <p>Нажимая "Записаться", я выражаю свое согласие с обработкой моих персональных данных в соответсвии с принятой <a href="#">политикой конфиденциальности</a> и принимаю <a href='#'>пользовательское соглажение</a>.</p>
                </div>
            </div>
        )
    }
}

ApplicationForm.propTypes = {
    cities: PropTypes.array.isRequired,
    cityID: PropTypes.string,
    onCitySelect: PropTypes.func.isRequired,
    datetime: PropTypes.object,
    onSendApplication: PropTypes.func.isRequired,
    getCities: PropTypes.func.isRequired,

    daylistPending: PropTypes.bool.isRequired,
    citiesPending: PropTypes.bool.isRequired,
};

export default ApplicationForm;