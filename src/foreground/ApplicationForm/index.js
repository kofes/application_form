import {connect} from 'react-redux';

import ApplicationForm from './ApplicationForm';

import {
    getDayList,
} from "../../core/actions/daylist";
import {
    getCities,
    selectCity,
} from "../../core/actions/cities";

import {
    sendApplication
} from '../../core/actions/application';

const mapStateToProps = state => ({
    cities: state.cities.data,
    exception: state.cities.exception,
    error: state.cities.error,
    cityID: state.cities.cityID,
    datetime: state.daylist.data,
    daylistPending: state.daylist.pending,
    citiesPending: state.cities.pending,
});

const mapDispatchToProps = dispatch => ({
    onCitySelect: cityID => {
        dispatch(selectCity(cityID));
        dispatch(getDayList(cityID));
    },
    onSendApplication: data => {
        dispatch(sendApplication(data));
    },
    getCities: () => {
        dispatch(getCities());
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationForm);