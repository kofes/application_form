import React, {Component} from 'react';
import PropTypes from 'prop-types';

import { camelToSnake } from '../../core/util';

import arrow from './arrow.png';
import './Dropdown.styl';

class Dropdown extends Component {
    constructor(props) {
        super(props);

        let selectedOption = -1;
        const { selected } = this.props;
        if (selected !== undefined && selected !== null && selected >= 0) {
            selectedOption = selected;
        }

        if (props.data.length) {
            if (selectedOption < 0)
                selectedOption = 0;
            this.props.onSelect(selectedOption);
        }

        this.state = {
            selected: selectedOption,
            open: false,
            valid: true,
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const { selected } = this.props;
        if (prevProps.selected !== this.props.selected) {
            this.setState({
                selected
            });
            if (selected >= 0) {        
                this.props.onSelect(selected);
            }
        }
    }

    onChange(index) {
        this.setState({
            ...this.state,
            open: false,
            selected: index,
            valid: true,
        });
        this.props.onSelect(index);
    }

    handleClickOutside(event) {
        const { validation, onValidationFail } = this.props;
        let valid = true;
        if (validation && this.state.selected < 0) {
            valid = false;
            onValidationFail();
        }
        this.setState({
            open: false,
            valid
        });
    }

    render() {
        const { data, placeholder, clickable } = this.props;
        const { selected, open, valid } = this.state;
        const name = camelToSnake(this.constructor.name);

        const dropdownHeader = (
            <div
                className={`${name}_header${open ? ' open' : ''}${!valid ? ' invalid' : ''}`}
                onClick={() => this.setState({
                    open: !this.state.open
                })}
            >
                <div className={`${name}_value`}>
                    {
                        selected >= 0 ?
                            data[selected] :
                            <span className={`${name}_placeholder`}>{placeholder}</span>
                    }
                </div>
                <img
                    className={`${name}_arrow${open ? ' open' : ''}`}
                    src={arrow}
                    alt=''
                    />
            </div>
        );
        const dropdownList = open ? (
            <ul className={`${name}_list`}>{
                data.map((elem, idx) =>
                    <li
                        key={idx}
                        onClick={() => this.onChange(idx)}
                    >{elem}</li>
                )
            }
            </ul>
        ): <React.Fragment/>;
        return (
            <div className={`${name}${!clickable ? ' disabled' : ''}`}
                 tabIndex='0'
                 onBlur={(event) => this.handleClickOutside(event)}>
                {dropdownHeader}
                {dropdownList}
            </div>
        )
    }
}

Dropdown.propTypes = {
    data: PropTypes.array.isRequired,
    placeholder: PropTypes.string,
    onSelect: PropTypes.func,
    selected: PropTypes.number,
    clickable: PropTypes.bool,
    validation: PropTypes.bool,
    onValidationFail: PropTypes.func,
};

Dropdown.defaultProps = {
    data: [],
    clickable: false,
    selected: -1,
    onSelect: index => {},
    validation: true,
    onValidationFail: () => {},
};

export default Dropdown;