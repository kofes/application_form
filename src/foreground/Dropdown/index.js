import {connect} from 'react-redux';

import Dropdown from './Dropdown';

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Dropdown);