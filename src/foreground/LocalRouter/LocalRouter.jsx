import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import ApplicationForm from '../ApplicationForm';
import Orders from '../Orders';

class LocalRouter extends Component {
    render() {
        return (
            <BrowserRouter>
                <Route exact path="/" component={ApplicationForm} />
                <Route path="/orders" component={Orders} />
            </BrowserRouter>
        )
    }
}

export default LocalRouter;
