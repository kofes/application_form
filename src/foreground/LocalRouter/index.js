import { connect } from 'react-redux';

import LocalRouter from "./LocalRouter";

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});
export default connect(mapStateToProps, mapDispatchToProps)(LocalRouter);