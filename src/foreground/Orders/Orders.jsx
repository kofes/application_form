import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './Orders.styl';

import {camelToSnake, formatPhone, formatDate} from '../../core/util';


class Orders extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { getOrders } = this.props;
        setInterval(() => getOrders(), 2000);
    }

    removeOrder(id) {
        const { removeOrder } = this.props;
        removeOrder(id);
    }

    render() {
        const { data, pending } = this.props;

        const headerLayout = (
            <tr>
                <th>Дата</th>
                <th>Время</th>
                <th>Город</th>
                <th>Дом</th>
                <th>Контактные номера</th>
                <th>Цена, ₽</th>
                <th>Действие</th>
            </tr>
        );
        const contentLayout = (
            <React.Fragment>
                {Object.entries(data).map(elem => {
                    const [key, value] = elem;
                    const {city, date, time} = value;
                    return (
                        <tr key={key}>
                            <td>{date}</td>
                            <td>{time}</td>
                            <td>{city.name}</td>
                            <td>{city.address}</td>
                            <td className='phones'>{city.phones.map(phone => <p>{formatPhone(phone)}</p>)}</td>
                            <td>{city.price}</td>
                            <td
                                className='remove'
                                onClick={() => this.removeOrder(key)}
                            >Удалить</td>
                        </tr>
                    );
                })}
            </React.Fragment>
        );

        return (
            <div className={camelToSnake(this.constructor.name)}>
                <table>
                    <tbody>
                    {headerLayout}
                    {contentLayout}
                    </tbody>
                </table>
                {pending ? <p className='loading'>Загрузка...</p> : null}
                {!Object.entries(data).length && !pending ? (<p className='no_orders'>У вас нет записей...</p>) : null}
            </div>
        );
    }
}

Orders.propTypes = {
    pending: PropTypes.bool.isRequired,
    data: PropTypes.object.isRequired,
    getOrders: PropTypes.func.isRequired,
    removeOrder: PropTypes.func.isRequired,
};

export default Orders;