import { connect } from 'react-redux';

import Orders from './Orders';

import {
    getApplications,
    removeApplication,
} from '../../core/actions/application';

const mapStateToProps = state => ({
    data: state.applications.data,
    pending: state.applications.pending,
});
const mapDispatchToProps = dispatch => ({
    getOrders: () => dispatch(getApplications()),
    removeOrder: id => dispatch(removeApplication(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Orders);