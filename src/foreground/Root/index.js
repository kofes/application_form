import { hot } from 'react-hot-loader';
import { connect } from 'react-redux';

import Root from './Root';

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

const App = connect(mapStateToProps, mapDispatchToProps)(Root);
export default (process.env.NODE_ENV === 'development' ? hot(module)(App) : App);