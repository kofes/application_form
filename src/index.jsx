import React from 'react';
import ReactDOM from 'react-dom';
import Root from './foreground/Root';
import store from './core/store';

ReactDOM.render(
    <Root store={ store } />,
    document.getElementById('root')
);