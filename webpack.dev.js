const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');

const SRC_DIR = path.resolve(__dirname, './src');
const DEV_DIR = path.resolve(__dirname, './dev');

module.exports = merge(common, {
    entry: ['react-hot-loader/patch', `${SRC_DIR}/index.jsx`],
    output: {
        path: DEV_DIR,
        filename: 'bundle.js',
    },
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: DEV_DIR,
        hot: true,        
        historyApiFallback: true,
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebPackPlugin({
            title: "Application page",
            favicon: `${SRC_DIR}/favicon.svg`,
            template: `${SRC_DIR}/index.ejs`,
            filename: 'index.html'
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
            'process.env.DEBUG': JSON.stringify(process.env.DEBUG)
        })
    ]
});