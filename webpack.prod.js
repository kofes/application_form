const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const BrotliPlugin = require('brotli-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const LoadashReplacementPlugin = require('lodash-webpack-plugin');

const SRC_DIR = path.resolve(__dirname, './src');
const BUILD_DIR = path.resolve(__dirname, './build');

module.exports = merge(common, {
    entry: `${SRC_DIR}/index.jsx`,
    output: {
        path: BUILD_DIR,
        filename: '[name].bundle.js',
    },
    mode: 'production',
    devtool: '',
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebPackPlugin({
            title: "Application page",
            favicon: `${SRC_DIR}/favicon.svg`,
            template: `${SRC_DIR}/index.ejs`,
            filename: 'index.html'
        }),
        new LoadashReplacementPlugin({
            'collections': true,
            'currying': true
        }),
        new BrotliPlugin({
            asset: '[path].br[query]',
            test: /\.(js|css|html|svg)$/,
            threshold: 10240,
            minRatio: 0.8
        }),
        new UglifyJsPlugin({
            cache: true,
            parallel: true,
            sourceMap: true
        })
    ]
});